#!/bin/bash

sudo add-apt-repository "deb http://archive.zentyal.org/zentyal 3.5 main extra"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 10E239FF
wget -q http://keys.zentyal.org/zentyal-3.5-archive.asc -O- | sudo apt-key add -

sudo apt-get update

sudo apt-get install -y zentyal

